from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=200)
    password = forms.CharField(label='Password', max_length=200, widget=forms.PasswordInput)


class SystemNoticeForm(forms.Form):
    message = forms.CharField(label='Message', widget=forms.Textarea)


class PasswordResetForm(forms.Form):
    password1 = forms.CharField(label='Password', max_length=200, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat', max_length=200, widget=forms.PasswordInput)


class CreateUserForm(forms.Form):
    secret = forms.CharField(label='Shared Secret', max_length=200, widget=forms.PasswordInput)
    name = forms.CharField(label='Name (local part without @)', max_length=200)
    password1 = forms.CharField(label='Password', max_length=200, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat', max_length=200, widget=forms.PasswordInput)


class DeleteUserForm(forms.Form):
    user = forms.CharField(label='Confirm user name', max_length=200)


class UserPasswordForm(forms.Form):
    oldpass = forms.CharField(label='Current password', max_length=200, widget=forms.PasswordInput)
    newpass1 = forms.CharField(label='New password', max_length=200, widget=forms.PasswordInput)
    newpass2 = forms.CharField(label='Repeat', max_length=200, widget=forms.PasswordInput)


class UserDeviceRevokationForm(forms.Form):
    password = forms.CharField(label='Password', max_length=200, widget=forms.PasswordInput)
