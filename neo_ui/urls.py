from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name=''),
    path('index', views.index, name='index'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),

    path('create-user', views.usercreate, name='create-user'),
    path('user/<str:name>', views.userdetail, name='user'),
    path('user/<str:name>/reset-password', views.password, name='reset-password'),
    path('user/<str:name>/delete', views.userdelete, name='user-delete'),
    path('system-notice', views.system_notice, name='system-notice'),

    path('password', views.user_password, name='user-password'),
    path('device/<str:device>/revoke', views.user_revoke_device, name='user-revoke-device'),
]
