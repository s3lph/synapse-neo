from typing import Optional

import os
import urllib.request
import urllib.error
import urllib.parse
import json
import socket
import hmac
import hashlib
import functools
from datetime import datetime

from .models import User


SYNAPSE_API_BASE = os.environ.get('NEO_API_BASE', 'http://localhost:8008')


def synapse_request(query, request, data=None, auth_header=True, method=None):
    url = os.path.join(SYNAPSE_API_BASE, query[1:])
    req = urllib.request.Request(url, method=method)
    if auth_header and 'synapse_token' in request.session:
        token = request.session['synapse_token']
        req.add_header('Authorization', f'Bearer {token}')
    req.add_header('Content-Type', 'application/json')
    req.add_header('User-Agent', 'Neo/0.1 (Synapse Admin Panel)')
    try:
        res = urllib.request.urlopen(req, json.dumps(data).encode() if data else None)
        return res.code, json.loads(res.read().decode())
    except urllib.error.HTTPError as e:
        return e.code, json.loads(e.read().decode())


def test_is_admin(request):
    # Append an "x" to the current user's name, as they can query their own information
    url = f'/_synapse/admin/v1/whois/{urllib.parse.quote(request.session["username"])}x'
    code, userlist = synapse_request(url, request)
    return code != 403


def get_userlist(request):
    url = f'/_synapse/admin/v1/users/{urllib.parse.quote(request.session["username"])}'
    code, userlist = synapse_request(url, request)
    if code != 200:
        return None
    # "Deleted" (i.e. deactivated) users have their password hash set to null
    return [User(u['name'], u['admin']) for u in userlist if u['password_hash'] is not None]


def get_userdetail(request, name):
    url = f'/_synapse/admin/v1/whois/{urllib.parse.quote(name)}'
    code, details = synapse_request(url, request)
    if code != 200:
        return None
    details = {
        'name': details['user_id'],
        'sessions': sorted([
            {
                'device': device,
                'address': connection['ip'],
                'hostname': reverse_lookup(connection['ip']),
                'user_agent': connection['user_agent'],
                'last_seen': datetime.fromtimestamp(int(connection['last_seen']) / 1000)
            }
            for (device, ddata) in details['devices'].items()
            for session in ddata['sessions']
            for connection in session['connections']
        ], key=lambda x: x['last_seen'], reverse=True)
    }
    return details


def get_user_sessions(request):
    url = '/_matrix/client/r0/devices'
    code, devices = synapse_request(url, request)
    if code != 200:
        return None
    devices = devices['devices']
    now = datetime.now()

    for device in devices:
        if device.get('last_seen_ip') is not None:
            device['hostname'] = reverse_lookup(device['last_seen_ip'])
        if device.get('last_seen_ts') is not None:
            device['last_seen'] = datetime.fromtimestamp(int(device['last_seen_ts']) / 1000)
        else:
            device['last_seen'] = now
    return sorted(devices, key=lambda x: x['last_seen'] if 'last_seen' in x else now, reverse=True)


def get_user_device(request, device):
    url = f'/_matrix/client/r0/devices/{urllib.parse.quote(device)}'
    code, device = synapse_request(url, request)
    if code != 200:
        return None
    if device.get('last_seen_ip') is not None:
        device['hostname'] = reverse_lookup(device['last_seen_ip'])
    if device.get('last_seen_ts') is not None:
        device['last_seen'] = datetime.fromtimestamp(int(device['last_seen_ts']) / 1000)
    return device


@functools.lru_cache(maxsize=128)
def reverse_lookup(address: Optional[str]) -> Optional[str]:
    if address is None:
        return None
    try:
        reverse = socket.gethostbyaddr(address)
        return reverse[0]
    except OSError:
        return None


def usercreate_hmac(
        secret: str,
        nonce: str,
        user: str,
        password: str,
        admin: bool = False,
        user_type: Optional[str] = None):
    mac = hmac.new(secret.encode('utf8'), digestmod=hashlib.sha1)
    mac.update(nonce.encode('utf8'))
    mac.update(b"\x00")
    mac.update(user.encode('utf8'))
    mac.update(b"\x00")
    mac.update(password.encode('utf8'))
    mac.update(b"\x00")
    mac.update(b"admin" if admin else b"notadmin")
    if user_type:
        mac.update(b"\x00")
        mac.update(user_type.encode('utf8'))
    return mac.hexdigest()
