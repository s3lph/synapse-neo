import urllib.parse

from django.shortcuts import render, redirect
from django.contrib import messages

from .forms import *
from .util import *


def index(request):
    if 'synapse_token' not in request.session:
        return redirect('login')

    devices = get_user_sessions(request)
    context = {
        'session': request.session,
        'devices': devices,
        'password_form': UserPasswordForm()
    }

    if request.session['is_admin']:
        users = get_userlist(request)
        context.update({
            'create_user_form': CreateUserForm(),
            'system_notice_form': SystemNoticeForm(),
            'userlist': users
        })
    return render(request, 'index.html', context)


def login(request):
    if 'synapse_token' in request.session:
        messages.success(request, 'Already logged in.')
        return index(request)
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if not form.is_valid():
            return redirect('login')
        login_request = {
            'type': 'm.login.password',
            'identifier': {
                'type': 'm.id.user',
                'user': form.cleaned_data['username']
            },
            'password': form.cleaned_data['password'],
            'initial_device_display_name': '[neo]'
        }
        code, login_response = synapse_request('/_matrix/client/r0/login', request, login_request)
        if code != 200:
            messages.error(request, 'Login failed')
            return redirect('login')
        request.session['username'] = login_response['user_id']
        request.session['synapse_token'] = login_response['access_token']
        request.session['is_admin'] = test_is_admin(request)
        return redirect('index')
    else:
        return render(request, 'login.html', {'session': request.session, 'form': LoginForm()})


def logout(request):
    if 'synapse_token' in request.session:
        synapse_request('/_matrix/client/r0/logout', request, {})
        request.session['is_admin'] = False
        del request.session['username']
        del request.session['synapse_token']
        messages.success(request, 'Successfully logged out.')
    return redirect('login')


def userdetail(request, name):
    if 'synapse_token' not in request.session:
        messages.error(request, f'Please log in first.')
        return redirect('login')
    if not request.session['is_admin']:
        messages.error(request, 'You are not an admin user.')
        return redirect('index')
    details = get_userdetail(request, name)
    context = {
        'session': request.session,
        'user': details,
        'pwreset_form': PasswordResetForm(),
        'delete_user_form': DeleteUserForm()
    }
    return render(request, 'user.html', context)


def password(request, name):
    if 'synapse_token' not in request.session:
        messages.error(request, f'Please log in first.')
        return redirect('login')
    if not request.session['is_admin']:
        messages.error(request, 'You are not an admin user.')
        return redirect('index')
    if request.method != 'POST':
        return redirect('user', name=name)
    form = PasswordResetForm(request.POST)
    if not form.is_valid():
        return redirect('user', name=name)
    if form.cleaned_data['password1'] != form.cleaned_data['password2']:
        return redirect('user', name=name)
    password_reset_request = {
        'new_password': form.cleaned_data['password1']
    }
    url = f'/_synapse/admin/v1/reset_password/{urllib.parse.quote(name)}'
    code, res = synapse_request(url, request, password_reset_request)
    if code != 200:
        messages.error(request, f'Failed to reset password for {name}: {code}')
        return redirect('user', name=name)
    messages.success(request, f'Successfully reset password for {name}')
    return redirect('user', name=name)


def usercreate(request):
    if 'synapse_token' not in request.session:
        messages.error(request, 'Please log in first.')
        return redirect('login')
    if not request.session['is_admin']:
        messages.error(request, 'You are not an admin user.')
        return redirect('index')
    if request.method != 'POST':
        return redirect('index')
    form = CreateUserForm(request.POST)
    if not form.is_valid():
        return redirect('index')
    if form.cleaned_data['password1'] != form.cleaned_data['password2']:
        return redirect('index')
    url = f'/_matrix/client/r0/register/available?username={urllib.parse.quote(form.cleaned_data["name"])}'
    acode, available_response = synapse_request(url, request, None)
    if acode != 200 or not available_response['available']:
        return redirect('index')
    ncode, nonce_response = synapse_request('/_synapse/admin/v1/register', request, None)
    if ncode != 200:
        return redirect('index')
    nonce = nonce_response['nonce']
    mac = usercreate_hmac(
        secret=form.cleaned_data['secret'],
        nonce=nonce,
        user=form.cleaned_data['name'],
        password=form.cleaned_data['password1'],
        admin=False)
    create_user_request = {
        'nonce': nonce,
        'username': form.cleaned_data['name'],
        'password': form.cleaned_data['password1'],
        'admin': False,
        'mac': mac
    }
    synapse_request('/_synapse/admin/v1/register', request, create_user_request)
    return redirect('index')


def userdelete(request, name):
    if 'synapse_token' not in request.session:
        messages.error(request, 'Please log in first.')
        return redirect('login')
    if not request.session['is_admin']:
        messages.error(request, 'You are not an admin user.')
        return redirect('index')
    if request.method != 'POST':
        messages.error(request, 'This endpoint only accepts POST request.')
        return redirect('user', name=name)
    form = DeleteUserForm(request.POST)
    if not form.is_valid():
        messages.error(request, 'Invalid form data.')
        return redirect('user', name=name)
    if form.cleaned_data['user'] != name:
        messages.error(request, f'Entered name does not match "{name}".')
        return redirect('user', name=name)
    user_delete_request = {
        'erase': True
    }
    url = f'/_synapse/admin/v1/deactivate/{urllib.parse.quote(name)}'
    code, delete_response = synapse_request(url, request, user_delete_request)
    if code != 200:
        messages.error(request, f'Failed to delete user "{name}": "{code}".')
        return redirect('index')
    messages.success(request, f'Deleted user "{name}".')
    return redirect('index')
    

def system_notice(request):
    if 'synapse_token' not in request.session:
        messages.error(request, 'Please log in first.')
        return redirect('login')
    if not request.session['is_admin']:
        messages.error(request, 'You are not an admin user.')
        return redirect('index')
    if request.method != 'POST':
        return redirect('index')
    form = SystemNoticeForm(request.POST)
    if not form.is_valid():
        return redirect('index')
    users = get_userlist(request)
    count = len(users)
    fail = 0
    for user in users:
        system_notice_request = {
            'user_id': user.name,
            'content': {
                'msgtype': 'm.text',
                'body': form.cleaned_data['message']
            }
        }
        code, res = synapse_request('/_synapse/admin/v1/send_server_notice', request, system_notice_request)
        if code != 200:
            fail += 1
    if fail > 0:
        messages.error(request, f'Failed to send System Notice to {fail}/{count} users.')
    else:
        messages.success(request, f'Sent System Notice to all {count} users.')
    return redirect('index')


def user_password(request):
    if 'synapse_token' not in request.session:
        messages.error(request, 'Please log in first.')
        return redirect('login')
    if request.method != 'POST':
        return redirect('index')
    form = UserPasswordForm(request.POST)
    if not form.is_valid():
        return redirect('index')
    if form.cleaned_data['newpass1'] != form.cleaned_data['newpass2']:
        return redirect('index')
    code, res = synapse_request('/_matrix/client/r0/capabilities', request, None)
    if code != 200 or not res['capabilities']['m.change_password']['enabled']:
        messages.error(request, 'Changing passwords is disabled on this homeserver')
        return redirect('index')

    password_change_request = {
        'new_password': form.cleaned_data['newpass1']
    }
    code, res = synapse_request('/_matrix/client/r0/account/password', request, password_change_request)
    if code == 401 and 'session' in res:
        password_change_request['auth'] = {
            'type': 'm.login.password',
            # Synapse seems to violate spec here...
            'user': request.session['username'],
            'identifier': {
                'type': 'm.id.user',
                'user': request.session['username']
            },
            'password': form.cleaned_data['oldpass'],
            'session': res['session']
        }
        code, res = synapse_request('/_matrix/client/r0/account/password', request, password_change_request)
    if code != 200:
        errormsg = f': {res["error"]}' if 'error' in res else ''
        messages.error(request, f'Failed to change password{errormsg}')
        return redirect('index')
    messages.success(request, f'Successfully changed password')
    return redirect('index')


def user_revoke_device(request, device):
    if 'synapse_token' not in request.session:
        messages.error(request, 'Please log in first.')
        return redirect('login')
    if request.method == 'POST':
        form = UserDeviceRevokationForm(request.POST)
        if not form.is_valid():
            return redirect('index')
        url = f'/_matrix/client/r0/devices/{urllib.parse.quote(device)}'
        code, res = synapse_request(url, request, {}, method='DELETE')
        if code == 401 and 'session' in res:
            device_revocation_request = {
                'auth': {
                    'type': 'm.login.password',
                    # Synapse seems to violate spec here...
                    'user': request.session['username'],
                    'identifier': {
                        'type': 'm.id.user',
                        'user': request.session['username']
                    },
                    'password': form.cleaned_data['password'],
                    'session': res['session']
                }
            }
            code, res = synapse_request(url, request, device_revocation_request, method='DELETE')
        if code != 200:
            errormsg = f': {res["error"]}' if 'error' in res else ''
            messages.error(request, f'Failed to revoke device{errormsg}')
            return redirect('index')
        messages.success(request, f'Successfully revoked device')
        return redirect('index')
    else:
        device_sesion = get_user_device(request, device)
        context = {
            'session': request.session,
            'form': UserDeviceRevokationForm(),
            'device': device_sesion
        }
        return render(request, 'device_revocation.html', context)
