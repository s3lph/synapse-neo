from django.apps import AppConfig


class NeoUiConfig(AppConfig):
    name = 'neo_ui'
